import sublime, sublime_plugin

class FindYamlAnchorCommand(sublime_plugin.TextCommand):
    def run(self, edit):

        v = self.view
        dest = v.substr(v.sel()[0])
        region_anch = v.find("&" + dest, 0)

        if region_anch:
            v.show(region_anch)
        else:
            err = 'Not found anchor'

            sublime.status_message(err)
